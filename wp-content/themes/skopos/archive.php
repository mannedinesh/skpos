<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();  ?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery( ".sect_btn" ).click(function() {
      var effect = 'slide';
    var options = { direction: 'left' };
     var duration = 500;
     jQuery('.category_nav').toggle(effect, options, duration);
        jQuery( ".closeIt" ).show(); 
        jQuery( ".buttons" ).hide();
    });

jQuery(".closeIt").click(function (e) {
     var duration = 500;
   jQuery('.category_nav').hide('slide', {direction: 'left'}, duration);
      jQuery( ".buttons" ).show(); 
      jQuery( ".closeIt" ).hide();
    });
});
 jQuery( window ).load(function() {
         jQuery('li.menu-item-19').addClass('current-menu-item');
     });  
</script>
<div class="our_work">
<section class="inner_banner">
	<div class="container">
        <div class="row">
            <div class="srvs_dtl">
          
                    <?php 
                    $the_query = new WP_Query( 'page_id=7' );

                    while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <h2><?php the_title();?></h2>
                    <?php the_content();?>
                    <?php endwhile;?>
                </div>
            </div>
        </div>
    </figure>
</section>
<div class="clear"></div>
<section class="our_wrk_lst_pan">
	<div class="sector_pan">
	<a  class="sect_btn">SECTOR <img src="<?php echo get_template_directory_uri(); ?>/images/button_icon.png" alt="" class="buttons"></a>
<div class="category_nav" style="display:none;">
<?php
$cat = get_terms('cat_casestudies'); ?>
<ul>
<li><a href="<?php echo get_permalink(); ?>">all</a></li>
<?php foreach($cat as $cat_casestudies) {
  $case = new WP_Query(array('post_type' => 'casestudies','post_per_page'=>-1,'taxonomy'=>'cat_casestudies','term' => $cat_casestudies->slug,
  ));
  $link = get_term_link(intval($cat_casestudies->term_id),'cat_casestudies');
   ?> 
    <li><a href="<?php echo $link; ?>" <?php if ($_GET['cat_casestudies']==$cat_casestudies->slug){ echo 'class="active"';} ?>><?php echo $cat_casestudies->name; ?></a></li>

<?php }
?>
<li><img src="<?php echo get_template_directory_uri(); ?>/images/button_icon_lft.png" alt="" class="closeIt"></li>
</ul>
</div>
</div>
<ul>
<?php while (have_posts()) : the_post();?>
<li>
<a href="<?php echo get_permalink();?>">
<img src="<?php the_field('landing_image');?>" alt="">
<div>
<span>
<?php the_title();?>
<?php the_content();?>
</span>
<figure><img src="<?php the_field('hover_image');?>" alt=""></figure>
</div>
</a>
</li>
<?php endwhile;?>
    </ul>
	</section>
<div class="clear"></div>
    </div>
<?php get_footer();?>
