<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<script type="text/javascript"> var j = jQuery.noConflict();
j(document).ready(function(){
  j('.slider1').bxSlider({
    slideWidth: 300,
    minSlides: 1,
    maxSlides: 1,
    infiniteLoop: false,
    pager: false,
    slideMargin: 10
  });
});
</script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.scrollToTop').click(function(){
            jQuery('html,body').animate({scrollTop: jQuery(".wrk_team_detail").offset().top-100}, 'slow');
        return false;
         });
         jQuery('.foundationTop').click(function(){
            jQuery('html,body').animate({scrollTop: jQuery(".wrk_team_result").offset().top-100}, 'slow');
        return false;
         });
    });
    jQuery( window ).load(function() {
         jQuery('li.menu-item-19').addClass('current-menu-item');
     });  
    </script>
<script src="<?php echo get_template_directory_uri(); ?>/js/js/jquery.jcarousel.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/js/jquery.js"></script>
<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css" rel="stylesheet" />
<div class="our_wrk_detail">
<section class="inner_banner">
	
	<figure><?php the_post_thumbnail('full');  ?>
    	<div class="container">
        	<div class="row">
                <div class="inner_ban_txt">
                	<div class="about_title">
                    	<h2><?php the_title();?></h2>
                    	<?php while (have_posts()) : the_post(); ?>
                              <?php the_content();?>
                <?php endwhile;?>
                	</div>
                </div>
            </div>
        </div>
    </figure>

</section>

<div class="clear"></div>

<section class="abt_wrk_clnt">
	<div class="container">
    	<div class="row">
        	<div class="wrk_clnt">
            	<strong>Client</strong>
                <span><?php the_field('client');?></span>
            </div>
            
            <div class="wrk_group">
            	<strong>Sector</strong>
                <span><?php the_field('sector')?></span>
            </div>
            
            <div class="wrk_methods">
            	<strong>Research methods</strong>
                <span><?php the_tags('', ', ', '<br />'); ?></span>
            </div>
            
        </div>
    </div>
</section>

<div class="clear"></div>

<section class="wrk_team_detail">
	<div class="go_btm"><a href="#" class="scrollToTop"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/dwn_arw_4.png"></a></div>
    <div class="container">
    	<div class="row">
       	  <h2>The brief</h2>
            
          <?php the_field('the_brief');?>
            <div class="slider1">
            <?php 
$data = get_post_meta($post->ID);
$ss= explode("=",$data['brief_gallery'][0]);
$ss1= explode(",",$ss[1]);
//print_r($ss1);
$ss2 = preg_replace('/[^A-Za-z0-9\-]/', '', $ss1);
//print_r($ss2);exit;
//echo count($ss2);exit;
if($data['brief_gallery'][0] != ''){
for($i=0;$i < count($ss2);$i++) {
$image = wp_get_attachment_image_src($ss2[$i],('full') );
?>
  <?php if($image[0] != ''):?>
         
<div class="slide"><img id="test" src=<?php echo $image[0];?> /></div>
<?php endif;?>

<?php
}
}
else
    {?>
 <li></li> 
 
<?php }
  ?>
  </div>

        </div>
    </div>
    
</section>

<div class="clear"></div>


<section class="approach_pan">
	<figure class="abt_img"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/approach_img_1.jpg"></figure>
    <div class="container">
    	<div class="row">
        	<article class="srvs_cnt_pan">
                <h3>The approach</h3>
                <?php the_field('the_approach');?>
                </article>
        </div>
    </div>
</section>

<div class="clear"></div>

<section class="wrk_team_result">
	<div class="go_btm"><a href="#" class="foundationTop"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/dwn_arw_4.png"></a></div>
    <div class="container">
    	<div class="row">
       	  <h2>The results</h2>
            
          <?php the_field('the_results');?>
           </div>
            
        </div>
    </div>
    
</section>

<div class="clear"></div>
<?php
$id= $post->ID;
$args = array(
        'post_type'     => 'worklogos',
        'post_status'   => 'publish',
        'meta_query' => array(
            array(
                'key' => 'our_work',
                'value' => $id ,
                'compare' => '='
            )
        )
    );

    $getPosts = new WP_Query($args);
while ( $getPosts->have_posts() ) : $getPosts->the_post(); ?>
    <?php the_post_thumbnail(); ?>
    <?php endwhile;wp_reset_query();?>

<section class="social_media">
	<div class="container">
    	<div class="row">
        	<?php echo ot_get_option('social_links');?>
        </div>
    </div>
</section>

<div class="clear"></div>

<?php $postlist_args = array(
   'posts_per_page'  => -1,
   'orderby'         => 'menu_order',
   'order'           => 'DESC',
   'post_type'       => 'casestudies',
   'your_custom_taxonomy' => 'cat_casestudies'
); 
$postlist = get_posts( $postlist_args );

// get ids of posts retrieved from get_posts
$ids = array();
foreach ($postlist as $thepost) {
   $ids[] = $thepost->ID;
}
// get and echo previous and next post in the same taxonomy        
$thisindex = array_search($post->ID, $ids);
$previd = $ids[$thisindex-1];
$nextid = $ids[$thisindex+1];
$previouspost=get_post($previd);
$nextpost=get_post($nextid);
?>

<section class="our_wrk_lst_pan">
	<ul>
        <li>
            <a href="<?php echo get_permalink($previd); ?>">
                <?php $meta_values = get_post_meta($previd); ?>
                               <?php echo wp_get_attachment_image($meta_values['landing_image'][0],'full'); ?>
                <div>
                    <span>
                       <span>
                        <?php echo $previouspost->post_title;?>
                        <?php echo $previouspost->post_content;?>
                    </span>
                    </span>
                    <?php $meta_values = get_post_meta($previd); ?>
                    <figure><?php echo wp_get_attachment_image($meta_values['hover_image'][0],'full'); ?></figure>
                </div>
            </a>
        </li>
        
        <li>
            <a href="<?php echo get_permalink($nextid); ?>">
                <?php $meta_values = get_post_meta($nextid); ?>
                <?php echo wp_get_attachment_image($meta_values['landing_image'][0],'full'); ?>
                <div>
                    <span>
                        <?php echo $nextpost->post_title;?>
                        <?php echo $nextpost->post_content;?>
                    </span>
                <figure><?php echo wp_get_attachment_image($meta_values['hover_image'][0],'full'); ?></figure>
                </div>
            </a>
        </li>
    </ul>
</section>


<div class="clear"></div>
</div>
<?php get_footer();?>
