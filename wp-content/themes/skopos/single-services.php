<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.scrollToTop').click(function(){
            jQuery('html,body').animate({scrollTop: jQuery(".our_wrk_pan").offset().top-100}, 'slow');
        return false;
         });
    
    jQuery('.download').click(function(e){
          e.preventDefault();
          jQuery('.downloadp').show("slow");
      });
    jQuery('.downloadless').click(function(e){
        e.preventDefault();
        jQuery('.downloadp').hide("slow");
    });
         
    });
     jQuery( window ).load(function() {
         jQuery('li.menu-item-21').addClass('current-menu-item');
     });  
    </script>
    <div class="service_detail">
	<section class="inner_banner">
	<?php while ( have_posts() ) : the_post(); ?>
    	<div class="container">
        	<div class="row">
                <div class="srvs_dtl">
                    <h2><?php the_title(); ?></h2>
                    <p><?php the_excerpt();?></p>
                </div>
            </div>
        </div>
    </figure>

</section>

<div class="clear"></div>

<section class="our_wrk_pan">
	
   
   <article class="our_works srvs_dtls_lst">
            	<figure class="go_btm"><a href="#" class="scrollToTop"><img src="<?php echo get_template_directory_uri(); ?>/images/dwn_arw_4.png" alt=""></a></figure>
                
				<?php the_content(); ?>
            </article>
    
</section>
<?php
  endwhile; wp_reset_query();  ?>

<div class="clear"></div>

<section class="download_sect">
	<div class="container">
           <div class="row"><div class="downloadp" style="display:none;"> <?php echo apply_filters('the_content',ot_get_option('downloadpdf'));?></div><div class="clear"> </div>
               <div><?php echo apply_filters('the_content',ot_get_option('download_text'));?></div>
              
           </div>
    	<!--<div class="row">
        	<div class="email">
            	<div class="cls_pop"><a href="#"><img src="images/button_icon_dwn.png" alt=""></a></div>
           	  <div class="email_sec_1">
                	Lorem ipsum dolor sit amet nommuny adipiscing elit. Sed do eiusmod tempor incidunt ut labore et.
                </div>
                <div class="email_submt">
                	  <?php //echo do_shortcode('[email-download download_id="1" contact_form_id="170"]'); ?>		
                </div>
            </div>
            
            <div class="clear"></div>
            
            <div class="downld">
                <div class="sec_1">whitepaper PDF (124kb)</div>
                <div class="sec_2"><a href="#"><img src="images/download_icon.png" alt=""></a></div>
                <div class="sec_3">Lorem ipsum dolor sit amet nommuny adipiscing elit. Sed do eiusmod tempor incidunt ut labore et.</div>
            </div>
        </div>-->
    </div>
</section>

<div class="clear"></div>

<section class="get_in_tch">
	<figure class="abt_img"><img src="<?php echo get_template_directory_uri(); ?>/images/img_5.jpg" alt=""></figure>
	<div class="container">
    	<div class="row">
        	<article class="cnt_frm">
            	<h2>Get in touch</h2>
                <?php echo do_shortcode( '[contact-form-7 id="169" title="Get in touch"]' ); ?>    
            	
            </article>
        </div>
    </div>
</section>
    </div>
<div class="clear"></div>

<?php
get_footer();
