<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

		<footer>
	<div class="container">
    	<div class="row">
       	  <?php echo ot_get_option('footer');?>
            
          <div class="clear"></div>
            
          <div class="pwrd_by">Website by Boxed Rocket</div>
            
        </div>
    </div>
</footer>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.jscrollpane.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/customSelectBox.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/example.css" />
<link href="<?php echo get_template_directory_uri(); ?>/css/custom.css" rel="stylesheet" />
<link href="<?php echo get_template_directory_uri(); ?>/css/fonts.css" rel="stylesheet" />
<script src="<?php echo get_template_directory_uri(); ?>/js/js/jquery.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/js/jquery-ui.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jScrollPane.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/SelectBox.js"></script>


<script type="text/javascript">
    jQuery(function() {
        jQuery("select.custom").each(function() {					
            var sb = new SelectBox({
                selectbox: jQuery(this),
                height: 150,
                width: 200
            });
        });
    });

  jQuery(document).ready(function(){
  jQuery('.wht_we_do a').click(function(){
	//jQuery('.slide-out-div').addClass('open');
	//jQuery('.slide-out-div').toggle('slow');
	 //jQuery(".slide-out-div").slideToggle("slow");
	 //jQuery(".slide-out-div").css('right','-3px');


      });
  });
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/js/jquery.tabSlideOut.v1.3.js"></script>
    <script type="text/javascript">
        var K = jQuery.noConflict();
    K(function(){
        K('.slide-out-div').tabSlideOut({
            tabHandle: '.handle',                     //class of the element that will become your tab
            pathToTabImage: '<?php echo get_template_directory_uri(); ?>/images/test_drive_btn.jpg', //path to the image for the tab //Optionally can be set using css
            imageHeight: '222px',                     //height of tab image           //Optionally can be set using css
            imageWidth: '52px',                       //width of tab image            //Optionally can be set using css
            tabLocation: 'right',                      //side of screen where tab lives, top, right, bottom, or left
            speed: 600,                               //speed of animation
            action: 'click',                          //options: 'click' or 'hover', action to trigger animation
            topPos: '200px',                          //position from the top/ use if tabLocation is left or right
            leftPos: '20px', 
            height: '400px',//position from left/ use if tabLocation is bottom or top
            fixedPosition: true                      //options: true makes it stick(fixed position) on scroll
        });
     });

    </script>
    <div class="slide-out-div testdrive">
            <a class="handle" href="#">test drive</a>
            <h2>TestDrive Today</h2>
                <?php echo do_shortcode( '[contact-form-7 id="205" title="Test Drive Today"]' ); ?> 
        </div>
<?php wp_footer(); ?>

</body>
</html>
