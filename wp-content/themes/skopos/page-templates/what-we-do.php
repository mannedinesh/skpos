<?php
/**
 * Template Name: What We Do Page 
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();  ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.scrollToTop').click(function(){
            jQuery('html,body').animate({scrollTop: jQuery(".our_wrk_pan").offset().top-100}, 'slow');
        return false;
         });
         jQuery('.scrollTop').click(function(){
            var moveid=jQuery(this).attr('id');
            jQuery('html,body').animate({scrollTop: jQuery("."+moveid).offset().top-100}, 'slow');
        return false;
         });
    }); 
    </script>
<div class="what_we_do_page">
<section class="inner_banner">
	
	<figure><?php the_post_thumbnail('full');  ?>
    	<div class="container">
        	<div class="row">
                <div class="inner_ban_txt">
                	<div class="what_we_d">
                    	<h2><?php the_title();?></h2>
                       <?php while (have_posts()) : the_post(); ?>
                         <?php the_content();?>
                         <?php endwhile;?>
                   	</div>
                </div>
            </div>
        </div>
    </figure>

</section>

<div class="clear"></div>

<section class="our_wrk_pan">
	
   
   <article class="our_works what_we_do_lst">
            	<figure class="go_btm"><a href="#" class="scrollToTop"><img src="<?php echo get_template_directory_uri(); ?>/images/dwn_arw_4.png" alt=""></a></figure>
                
				<ul>
    <?php 
    $args=array(
  'post_type' => 'services',
  'post_status' => 'publish',
   'orderby' => 'DESC'
    );
$my_query=query_posts( $args ); $i=1; while ( have_posts() ) : the_post(); ?>
                                    
                                     
                	<li class="scrollTop<?php echo $i; ?>">
                    	<div class="lst_cnt">
                    		<figure><?php the_post_thumbnail('full');  ?></figure>
                        	<article>
                            	<p><?php the_title();?></p>
                                <span><?php the_excerpt();?></span>
                                <div class="more_btn_1">
                                <a href="<?php echo get_permalink($post->ID); ?>"><span>More</span>
                                    <?php if ($i % 2 == 0) { ?>
                                    <figure><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/button_icon_orng.png"></figure>
                                    <?php } else { ?>
                                     <figure><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/button_icon_gry.png"></figure>
                                    <?php } ?>
                                </a>
                                </div>
                            </article>
                        </div>
                   <figure class="go_btm"><a href="#" id="scrollTop<?php echo $i; ?>" class="scrollTop"><img src="<?php echo get_template_directory_uri(); ?>/images/dwn_arw.png" alt=""></a></figure></li> 
                     <?php
               $i++; endwhile;
                wp_reset_query();  ?>
                    
                </ul>
                
            </article>
    
</section>


<div class="clear"></div>
<section class="get_in_tch">
	<figure class="abt_img"><img src="<?php echo get_template_directory_uri(); ?>/images/img_5.jpg" alt=""></figure>
	<div class="container">
    	<div class="row">
        	<article class="cnt_frm">
            	<h2>Get in touch</h2>
                <?php echo do_shortcode( '[contact-form-7 id="169" title="Get in touch"]' ); ?>            	
            </article>
        </div>
    </div>
</section>
</div>
<div class="clear"></div>

<?php get_footer();?>
