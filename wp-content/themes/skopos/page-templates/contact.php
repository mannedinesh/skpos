<?php
/**
 * Template Name: Contact Page 
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();  ?>
<style>
     #map-canvas {
        height: 647px;
        margin: 0px;
        padding: 0px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script>
function initialize() {
	
  var bryantPark = new google.maps.LatLng(<?php echo ot_get_option('latitude');?>, <?php echo ot_get_option('longitude');?>);
  var panoramaOptions = {
    position: bryantPark,
    pov: {
      heading: 165,
      pitch: 0
    },
    zoom: 1
  };
  var myPano = new google.maps.StreetViewPanorama(
      document.getElementById('map-canvas'),
      panoramaOptions);
  myPano.setVisible(true);
}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    <div class="contact_page">
<section class="get_in_tch">
	<figure class="abt_img"><?php the_post_thumbnail('full');  ?></figure>
	<div class="container">
    	<div class="row">
        	<article class="cnt_frm">
            	<h2>Contact us to engage</h2>
                <?php echo do_shortcode( '[contact-form-7 id="169" title="Get in touch"]' ); ?> 
           </article>
        </div>
    </div>
</section>
<div class="clear"></div>
<section class="enq_pan">
	<div class="container">
    	<div class="row">
        	<div class="col-lg-6 col-sm-6 brdr_rht">
            	<?php echo ot_get_option('business_enquiries');?>
            </div>
            <div class="col-lg-6 col-sm-6">
            <?php echo ot_get_option('press_enquiries');?>
            </div>
            
      </div>
    </div>
</section>
<div class="clear"></div>
<section class="rstr_fr_tst">
<?php echo ot_get_option('register_testdrive');?>
</section>
<div class="clear"></div>
<section class="our_ofc">
	<div class="our_ofc_txt">
    	<h2>Our office</h2>
    </div>
	<div id="map-canvas"></div>
    
</section>
</div>
  
<?php get_footer();?>
