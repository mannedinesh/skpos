<?php
/**
 * Template Name: About template
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();  ?>

<script type="text/javascript"> var j = jQuery.noConflict();
j(document).ready(function(){
  j('.slider1').bxSlider({
    slideWidth: 300,
    minSlides: 2,
    maxSlides: 3,
    infiniteLoop: false,
    pager: false,
    slideMargin: 10
  });
});
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/js/jquery.jcarousel.min.js"></script>
<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css" rel="stylesheet" />
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.scrollToTop').click(function(){
            jQuery('html,body').animate({scrollTop: jQuery(".abt_pan").offset().top-100}, 'slow');
        return false;
         });
         jQuery('.managementTop').click(function(){
            jQuery('html,body').animate({scrollTop: jQuery(".our_wrk_pan").offset().top-100}, 'slow');
        return false;
         });
         jQuery('.scrollToTop3').click(function(){
            jQuery('html,body').animate({scrollTop: jQuery(".get_in_tch").offset().top-100}, 'slow');
        return false;
      });
    }); 
    </script>
<section class="inner_banner">
	
	<figure><?php the_post_thumbnail('full');  ?>
    	<div class="container">
        	<div class="row">
                <div class="inner_ban_txt">
                	<div class="about_title">
                    	<h2><?php the_title();?></h2>
                    	 <?php while (have_posts()) : the_post(); ?>
                         <?php the_content();?>
                         <?php endwhile;?>
                	</div>
                </div>
            </div>
        </div>
    </figure>

</section>

<div class="clear"></div>

<section class="abt_pan">
    	<article class="inr_adt_cnt">
      <div class="go_btm"><a href="#" class="scrollToTop"><img src="<?php echo get_template_directory_uri(); ?>/images/dwn_arw.png" alt=""></a></div>
         <?php echo ot_get_option('about_content');?>	
   </article>

  
</section>

<div class="clear"></div>

<section class="our_wrk_pan">
	
    <div class="container">
    	<div class="row">
        	<article class="our_works management_pan">
            	<figure class="go_btm"><a href="#" class="managementTop"><img src="<?php echo get_template_directory_uri(); ?>/images/dwn_arw_3.png" alt=""></a></figure>
                
				  <?php echo ot_get_option('foundation');?>
                
                <ul>
			 <?php 
    $args=array(
  'post_type' => 'staff',
  'post_status' => 'publish',
  'posts_per_page' => 3,
  'orderby' => 'menu_order'
    );
$my_query=query_posts( $args ); while ( have_posts() ) : the_post(); $meta_values = get_post_meta($post->ID); ?>		
                	<li>
                    	<figure><?php the_post_thumbnail('full');  ?></figure>
                        <strong><?php the_title();?></strong>
                        <span><?php echo $meta_values['title'][0]; ?></span>
                    </li>
                     <?php
                endwhile;
                wp_reset_query();  ?>
                  
                </ul>
                
                
                <div class="clear"></div>
                
             	<?php echo ot_get_option('delivering');?>
                
                <div class="staff_lst">
                	
       <div class="slider1">
                      <?php 
    $args=array(
  'post_type' => 'staff',
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'orderby' => 'menu_order'
    );
$my_query=query_posts( $args ); while ( have_posts() ) : the_post(); $meta_values = get_post_meta($post->ID); ?>		
                	<div class="slide">
                    	<figure><?php the_post_thumbnail('full');  ?></figure>
                        <strong><?php the_title();?></strong>
                        <span><?php echo $meta_values['title'][0]; ?></span>
                    </div>
                     <?php
                endwhile;
                wp_reset_query();  ?>
            </div>
                
                	
             	</div>
                
            </article>
        </div>
    </div>
    
</section>


<div class="clear"></div>


<section class="get_in_tch">
	<?php echo ot_get_option('getintouch');?>
</section>

<div class="clear"></div>

<?php get_footer();?>
