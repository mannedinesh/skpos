<?php
/**
 * Template Name: Test Drive Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();  ?>

    <div class="contact_page">
<section class="get_in_tch test-contact">
<div class="container">
    	<div class="row">
        	<article class="cnt_frm">
<h2>Test Drive Today</h2>
<?php echo do_shortcode('[contact-form-7 id="205" title="Test Drive Today"]');?>
</article>
</div>
</div>
</section>
</div>
  
<?php get_footer();?>
