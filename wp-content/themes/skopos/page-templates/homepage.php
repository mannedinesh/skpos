<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<script type="text/javascript"> var j = jQuery.noConflict();
j(document).ready(function(){

  j('.slider1').bxSlider({
    slideWidth: 200,
    minSlides: 2,
    maxSlides: 4,
    infiniteLoop: false,
    pager: false,
    slideMargin: 5
  });
  jQuery('.scrollToTop').click(function(){
            jQuery('html,body').animate({scrollTop: jQuery(".our_wrk_pan").offset().top-100}, 'slow');
        return false;
         });
    jQuery('.scrollToTop1').click(function(){
    jQuery('html,body').animate({scrollTop: jQuery(".abt_pan").offset().top-100}, 'slow');
return false;
    });
    jQuery('.scrollToTop2').click(function(){
            jQuery('html,body').animate({scrollTop: jQuery(".srvs_pan").offset().top-100}, 'slow');
        return false;
      });
});
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/js/jquery.jcarousel.min.js"></script>
<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css" rel="stylesheet" />
<section>
<div>

       
           <?php putRevSlider( "home_slider" ) ?>
       
            </div>
</section>

<div class="clear"></div>
<section class="abt_pan">

    <?php echo ot_get_option('fullcircle');?>
   
</section>
<div class="clear"></div>
<section class="our_wrk_pan">
	<div class="container">
    	<div class="row">
        	<article class="our_works">
            	<figure class="go_btm"><a href="#" class="scrollToTop"><img src="<?php echo get_template_directory_uri(); ?>/images/dwn_arw_2.png" alt=""></a></figure>
                <h3><?php echo ot_get_option('passionate');?></h3>
				
                
                <div class="our_mdl_pan slider1">
           
                <?php $args = array('post_type' => 'worklogos','posts_per_page' => '-1','order'=> 'ASC','orderby'=>'menu_order');
                                  $loop = new WP_Query( $args );
                                                                                        
                          while ( $loop->have_posts() ) : $loop->the_post();
                           echo $our_work = get_post_meta($post->ID, 'our_work',true);
                          ?>
                        	<div class="slide">
                    	<a href="<?php echo get_permalink($our_work);?> "><?php the_post_thumbnail();?></a>
                    </div>
                   
                        	<?php endwhile;?>
                </div>
                <div class="our_wrk_btn"><a href="<?php echo get_permalink('7');?>"><span>our work</span><figure><img src="<?php echo get_template_directory_uri(); ?>/images/button_icon_gry.png" alt=""></figure></a></div>
                 </article>
                 
                 
                </div>
               
               
        </div>
    </div>
</section>
<div class="clear"></div>
<section class="srvs_pan">
    <?php echo ot_get_option('standout_widget');?>
    
</section>
<div class="clear"></div>
<section class="get_in_tch">
        <?php echo ot_get_option('getintouch');?>
    </section>
<div class="clear"></div>
<?php get_footer();?>
