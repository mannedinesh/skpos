<?php
/**
 * Template Name: Blog listing
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();  ?>
<script type="text/javascript">
      
(function( $ ){
       $(document).ready( function(){
  $('#filters li a').click(function() {
     
    // get filter value from option value
    var filterValue = $(this).attr('data-filter');
       $('.'+filterValue).show();
       // use filterFn if matches value
	$('.blog_psts #containerpro').isotope({ filter: '.'+filterValue,layoutMode: 'fitRows' });
      
  });

	   });
  
})(jQuery);
 </script>
<div class="blog_page">
<section class="inner_banner">
	<div class="blog_nav">
    	<div class="container">
        	<div class="row">
                    <ul id="filters">
<?php
 $args = array(
 'hide_empty'=> 1,
 'orderby' => 'ID',
'order' => 'ASC'
 );

$categories = get_categories($args);

  foreach($categories as $category) { ?>
<li><a href="#<?php echo $category->slug; ?>" data-filter="<?php echo $category->slug; ?>"><?php echo $category->name; ?></a></li>
    
<?php } ?>
</ul>
                </div>
            </div>
        </div>
    </figure>

</section>

   
  
<section class="blog_psts">
	<div class="blog_title_pan">
    	<h2><?php the_title(); ?></h2>
        <?php echo $post->post_content; ?>
    </div>

      <ul id="containerpro">
 <?php 
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
     $the_query = new WP_Query(array(
      'post_type' => 'post',
      'post_status' => 'publish',
      'posts_per_page' => 3,
      'paged'=> $paged
        )); 
     while ( $the_query->have_posts() ) : 
     $the_query->the_post();
     $terms = get_the_terms( $post->ID, 'category' ); ?>

         <li class="<?php foreach( $terms as $term ) echo $term->slug; ?>"><figure><?php the_post_thumbnail('full');?></figure>
            <div class="blg_cnt">
            	<h2><?php the_title(); ?></h2><?php $content = $post->post_content; $trimmed_content = wp_trim_words( $content, 30 );?>
                <p><?php echo $trimmed_content; ?> </p>
                
            </div>
            
            <div class="more_btn_1">
                    <a href="<?php echo get_permalink($post->ID); ?>"><span>Read More</span><figure><img src="<?php echo get_template_directory_uri(); ?>/images/button_icon_gry.png" alt=""></figure></a>
            </div>
            
            <div class="blg_pst_dtls">
            	<div class="blg_pst_by">Author <span><?php echo get_the_author_meta('first_name'); ?> <?php echo get_the_author_meta('last_name'); ?></span> <?php echo get_the_author_meta('description'); ?></div>
                <div class="blg_pst_dt"><?php echo mysql2date('F d Y', $post->post_date); ?></div>
            </div></h1></li>

                   <?php 
    endwhile; 
  
?>  </ul>

 </section>


<div class="clear"></div>
<section class="download_sect">
	<div class="container">
    	<div class="row">
          	<div class="old_new_artcls">
            	<div class="old_artcles">
                	
                    	<figure><img src="<?php echo get_template_directory_uri(); ?>/images/gry_lft_arw.png" alt=""></figure>
           	    		<span><?php previous_posts_link( 'Older articles' ); ?></span>
                    
                </div>
                
                <div class="new_artcles">
                	
                    	<figure><img src="<?php echo get_template_directory_uri(); ?>/images/gry_rht_arw.png" alt=""></figure>
                        <span><?php next_posts_link( 'Newer articles', $the_query->max_num_pages ); ?></span>
                    
                </div>
                
          </div>
        </div>
    </div>
</section>
</div>
<div class="clear"></div>
<script  type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.isotope.min.js"></script>
<?php get_footer();?>
