<?php
/**
 * Template Name: Our Work Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery( ".sect_btn" ).click(function() {
      var effect = 'slide';
    var options = { direction: 'left' };
     var duration = 500;
     jQuery('.category_nav').toggle(effect, options, duration);
        jQuery( ".closeIt" ).show(); 
        jQuery( ".buttons" ).hide();
    });

jQuery(".closeIt").click(function (e) {
     var duration = 500;
   jQuery('.category_nav').hide('slide', {direction: 'left'}, duration);
      jQuery( ".buttons" ).show(); 
      jQuery( ".closeIt" ).hide();
    });
});
</script>
<script type="text/javascript"> var j = jQuery.noConflict();
j(document).ready(function(){
  j('.slider1').bxSlider({
    slideWidth: 200,
    minSlides: 2,
    maxSlides: 4,
    infiniteLoop: false,
    pager: false,
    slideMargin: 5
  });
});
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/js/jquery.jcarousel.min.js"></script>
<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css" rel="stylesheet" />
<div class="our_work">
<section class="inner_banner">
	<div class="container">
        <div class="row">
            <div class="srvs_dtl">
                    <h2><?php the_title();?></h2>
                    <?php while (have_posts()) : the_post(); ?>
                              <?php the_content();?>
                <?php endwhile;?>
                </div>
            </div>
        </div>
    </figure>

</section>

<div class="clear"></div>

<section class="our_wrk_lst_pan">

<div class="sector_pan">
	<a  class="sect_btn">SECTOR <img src="<?php echo get_template_directory_uri(); ?>/images/button_icon.png" alt="" class="buttons"></a>
<div class="category_nav" style="display:none;">
<?php
$cat = get_terms('cat_casestudies'); ?>
<ul>
<li><a href="<?php echo get_permalink(); ?>" class="active">all</a></li>
<?php foreach($cat as $cat_casestudies) {
  $case = new WP_Query(array('post_type' => 'casestudies','post_per_page'=>-1,'taxonomy'=>'cat_casestudies','term' => $cat_casestudies->slug,
  ));
  $link = get_term_link(intval($cat_casestudies->term_id),'cat_casestudies');
?> 
    <li><a href="<?php echo $link; ?>"><?php echo $cat_casestudies->name; ?></a></li>

<?php }
?>
<li><img src="<?php echo get_template_directory_uri(); ?>/images/button_icon_lft.png" alt="" class="closeIt"></li>
</ul>
</div>
</div>


<ul>
<?php $args = array('post_type' => 'casestudies','posts_per_page' => '-1','order'=> 'ASC','orderby'=>'menu_order');
      $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post();?>
	<li>
    	<a href="<?php echo get_permalink();?>">
       		<img src="<?php the_field('landing_image');?>" alt="">
            <div>
            	<span>
                	<?php the_title();?>
                	<?php the_content();?>
                </span>
            	<figure><img src="<?php the_field('hover_image');?>" alt=""></figure>
            </div>
        </a>
    </li>
    <?php endwhile;?>
    
    
</ul>
	
</section>

<div class="clear"></div>


<section class="our_wrk_pan">
	
    <div class="container">
    	<div class="row">
        	<article class="our_works">
            	<h3><?php echo ot_get_option('passionate');?></h3>
				
                	
                    <div class="our_mdl_pan slider1">
                    	
                        <?php $args = array('post_type' => 'worklogos','posts_per_page' => '-1','order'=> 'ASC','orderby'=>'menu_order');
                                  $loop = new WP_Query( $args );
                                                                                        
                          while ( $loop->have_posts() ) : $loop->the_post();
                           echo $our_work = get_post_meta($post->ID, 'our_work',true);
                          ?>
                        	<div class="slide">
                    	<a href="<?php echo get_permalink($our_work);?> "><?php the_post_thumbnail();?></a>
                    </div>
                        	<?php endwhile;?>
                    </div>
                                   
            </article>
        </div>
    </div>
    
</section>
</div>
<?php get_footer();?>

