<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<script type="text/javascript">
      jQuery( window ).load(function() {
         jQuery('li.menu-item-20').addClass('current-menu-item');
     });  
    </script>
<section class="inner_banner">
<figure><?php the_post_thumbnail('full');  ?></figure>
</section>

<div class="clear"></div>
<section class="blog_psts">
	<ul>
       <li><?php while ( have_posts() ) : the_post(); ?>
       		<div class="blg_cnt">
            	<h2><?php the_title(); ?></h2>
                <p>Author <span><?php echo get_the_author_meta('first_name'); ?> <?php echo get_the_author_meta('last_name'); ?></span> <?php echo get_the_author_meta('description'); ?>	<?php echo mysql2date('F d Y', $post->post_date); ?></p>
           <p>  <?php echo $post->post_content; ?></p>
                
                <div class="blg_pst_by_dtl">
                	<div class="blg_pst_by_dtl_lft">
                    	<span><?php echo get_the_author_meta('first_name'); ?> <?php echo get_the_author_meta('last_name'); ?></span>
                        <strong><?php echo get_the_author_meta('description'); ?></strong>
                        <strong><?php echo get_the_author_meta('email'); ?></strong>
                    </div>
                    
                    <div class="blg_pst_by_dtl_rht">
               	    	<?php echo do_shortcode( '[addtoany]' ); ?>
                    </div>
                </div>
                
            </div>
            <?php
                endwhile;  wp_reset_query(); ?>
           <div class="blg_pst_dtls">
                <?php // get_posts in same custom taxonomy
		
$categories = get_the_category($post->ID);
if ($categories) {
$category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
$args=array(
'post_type' => 'post',
'category__in' => $category_ids,
'post__not_in' => array($post->ID),
'posts_per_page'=> 1 

);
// get and echo previous and next post in the same taxonomy        
 query_posts($args);  
        while ( have_posts() ) : the_post();  ?>
            <div class="blg_pst_by">You may also like <span><a href="<?php echo get_permalink($post->ID); ?>"><?php the_title(); ?></a></span> <?php echo get_the_author_meta('first_name'); ?> <?php echo get_the_author_meta('last_name'); ?> <?php echo mysql2date('F d Y', $post->post_date); ?></div><?php  endwhile; wp_reset_query(); } ?>
           </div>
       </li>
     
    </ul>
	
</section>


<div class="clear"></div>
<?php  $postlist_args = array(
   'posts_per_page'  => -1,
   'orderby'         => 'menu_order',
   'order'           => 'DESC',
   'post_type'       => 'post',
    
); 
$postlist = get_posts( $postlist_args );

// get ids of posts retrieved from get_posts
$ids = array();
foreach ($postlist as $thepost) {
   $ids[] = $thepost->ID;
}
// get and echo previous and next post in the same taxonomy        
$thisindex = array_search($post->ID, $ids);
$previd = $ids[$thisindex-1];
$nextid = $ids[$thisindex+1];

?>
<section class="download_sect">
	<div class="container">
    	<div class="row">
        	<div class="old_new_artcls">
            	<div class="old_artcles">
                	<a href="<?php echo get_permalink($previd); ?>">
                    	<figure><img src="<?php echo get_template_directory_uri(); ?>/images/gry_lft_arw.png" alt=""></figure>
           	    		<span>PREVIOUS ARTICLE</span>
                    </a>
                </div>
                
                <div class="new_artcles">
                	<a href="<?php echo get_permalink($nextid); ?>">
                    	<figure><img src="<?php echo get_template_directory_uri(); ?>/images/gry_rht_arw.png" alt=""></figure>
                        <span>NEXT ARTICLE</span>
                    </a>
                </div>
                
          </div>
        </div>
    </div>
</section>

<div class="clear"></div>
	

<?php
get_footer();
